#!/usr/bin/python3

import sys
sys.path.append("/home/ubuntu/.local/lib/python3.8/site-packages")
import mysql.connector
import os
import datetime
import configparser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from cryptography.fernet import Fernet

config = configparser.ConfigParser()

config.read('dbBackupSettings.cfg')

#Gets database login information from config file
hostname = config.get('DATABASE','HOSTNAME')
username = config.get('DATABASE','USERNAME')
password = config.get('DATABASE','PASSWORD')
databaseName = config.get('DATABASE', 'DATABASENAME')

#Gets infos for backup creation of database from config file
datetimeFormat = config.get('FILESETTINGS', 'DATETIMEFORMAT')
createDBBackupFileName = config.get('FILESETTINGS', 'CREATEDBBACKUPFILENAME')
replaceTextTemplate = databaseName + '_'
backupFolderName = config.get('FILESETTINGS', 'BACKUPFOLDERNAME')
backupFileNameTemplate = config.get('FILESETTINGS', 'BACKUPFILENAMETEMPLATE')

#Creates a string for the current time with format from config
now = datetime.datetime.now().strftime(datetimeFormat)

#Gets logfile information from config file
logFileLocation = config.get('LOGGING', 'LOGFILELOCATION')
logFileName = config.get('LOGGING', 'LOGFILENAMETEMPLATE') + now
datetimeFormatLogging = config.get('LOGGING', 'DATETIMEFORMATLOGGING')

#Gets email sender and receiver from config
sender = config.get('EMAILSENDER', 'SENDER')
receivers = config.get('EMAILSENDER', 'RECEIVER')

#Gets secret location for password encryption
secretLocation = config.get('SECRET', 'SECRETLOCATION')

#Method to log action into logfile
def log_action(action):
    timestamp = datetime.datetime.now().strftime(datetimeFormatLogging)
    log_entry = f'{timestamp} - {action}\n'

    with open(logFileLocation + logFileName, 'a') as file:
        file.write(log_entry)


#Saves file from db backup
def saveDBBackupFile(fileName, databaseName, replaceDatabaseName, saveFolderName, backupFileNewName):
    #Replaces and writes new name for backup database
    with open(fileName, 'r') as file:
        data = file.read()
        data = data.replace(databaseName, replaceDatabaseName)

    with open(createDBBackupFileName, 'w') as file:
        file.write(data)

    #Copies and renames file to backup file directory
    os.system('sudo cp -R ' + fileName + ' ' + saveFolderName)
    os.system('sudo mv ' + saveFolderName + fileName + ' ' + saveFolderName + backupFileNewName)

    log_action('Created backup SQL file and saved it with the name: ' + backupFileNewName)


#Executes SQL File in Database
def executeSQLQuery(conn, dump_file):
    if conn.is_connected():
        cursor = conn.cursor()
        #Opens SQL-Dump file and executes it in database
        with open(dump_file, 'r') as file:
            query = file.read()
            try:
                cursor.execute(query)
                print('Anfrage erfolgreich ausgeführt:', query)
            except mysql.connector.Error as error:
                print('Fehler beim Ausführen der Anfrage:', query)
                print('Fehlermeldung:', error.msg)
                log_action(('Error occured: ', error.msg))

        cursor.close()
        conn.close()

        log_action('Database backup for database ' + databaseName + ' has been created in database under the name: ' + replaceTextTemplate + now)
    else:
        print('Verbindung fehlgeschlagen')
        log_action('Connection failed to database')


#Builds emal message with attachment and sends it to the receipient
def sendEmailWithAttachment(sender, receipient, databaseName, attachmentPath, attachmentFileName):
    #Builds email message and attaches recent backup file
    message = MIMEMultipart()
    message['From'] = 'No Reply <' + sender  +'>'
    message['To'] = 'Gabriel Varga <' + receipient + '>'
    message['Subject'] = 'Database Backup from ' + now

    message.attach(MIMEText('Dear Customer, a backup for the database \'' + databaseName + '\' has been created. Attached is the SQL-Dump from the database backup.'))

    attachment = MIMEApplication(open(attachmentPath, 'rb').read())
    attachment.add_header('Content-Disposition', 'attachment', filename=attachmentFileName)

    message.attach(attachment)

    #Sends email to receipient
    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receipient, message.as_string())
        print('Successfully sent email')
        log_action('Email has been sent to: ' + receipient)
    except smtplib.SMTPException as msg:
        print('Error: unable to send email ', msg)
        log_action(('Error: unable to send email ', msg))


def decrypt_string_with_aes():
    secretConfig = configparser.ConfigParser()
    secretConfig.read(secretLocation + '/secrets.cfg')
    f = Fernet(secretConfig.get('SECRETS', 'KEY'))
    decrypted_string = f.decrypt(password.encode('utf-8'))
    return decrypted_string.decode('utf-8')


#Creates SQL-Dump
os.system('sudo ./mysqldumpBashScript.sh ' + username  + ' ' + decrypt_string_with_aes() + ' ' + databaseName + ' ' + createDBBackupFileName)

log_action('SQL-Dump created for database: ' + databaseName)

saveDBBackupFile(createDBBackupFileName, databaseName, replaceTextTemplate + now, backupFolderName, backupFileNameTemplate + now + '.sql')

#Creates connection to database
conn = mysql.connector.connect(
    host=hostname,
    user=username,
    password=decrypt_string_with_aes(),
)

executeSQLQuery(conn, createDBBackupFileName)

backupFileName = backupFileNameTemplate + now + '.sql'

sendEmailWithAttachment(sender, receivers, databaseName, backupFolderName + backupFileName, backupFileName)
